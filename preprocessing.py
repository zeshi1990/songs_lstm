import random

import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from utils import read_lyrics

logs_path = '/tmp/tensorflow/rnn_words'
writer = tf.summary.FileWriter(logs_path)

arr, arr_1d, char_2_idx, idx_2_char = read_lyrics("lyrics.csv")
data = arr_1d

# Below are the sections for training and testing the RNN(LSTM) model for generating the lyrics
# of Drakes automatically
char_size = arr.shape[1]

# parameters
data_rep = 1
lr = 0.001
training_iters = 2000
display_step = 1000
n_inputs = 20
batch_size = 10

n_hidden = 512

# inputs
x = tf.placeholder("float", [None, n_inputs, 1])
y = tf.placeholder("float", [None, char_size])


weights = {
    "out": tf.Variable(tf.random_normal([n_hidden, char_size]))
}


biases = {
    "out": tf.Variable(tf.random_normal([char_size]))
}


# Below is the 1D version of, remember to implement a char_size-D version of the LSTM, this can be interesting
def RNN_1D(x, weights, biases):
    x = tf.split(tf.reshape(x, [-1, n_inputs]), n_inputs, 1)
    rnn_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(n_hidden), rnn.BasicLSTMCell(n_hidden)])
    outputs, states = rnn.static_rnn(rnn_cell, inputs=x, dtype=tf.float32)
    return tf.matmul(outputs[-1], weights['out']) + biases['out']


# see https://stackoverflow.com/questions/46734289/multivariate-input-for-lstm-in-tensorflow for example;
def RNN_ND(x, weights, biases):
    # x in put shape: (batch_size, time_step_size, input_vec_size)
    x = tf.transpose(x, [1, 0, 2])
    x = tf.split(tf.reshape(x, [-1, n_inputs * char_size]), n_inputs, 0)
    rnn_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(n_hidden), rnn.BasicLSTMCell(n_hidden)])
    outputs, states = rnn.static_rnn(rnn_cell, inputs=x, dtype=tf.float32)
    return tf.matmul(outputs[-1], weights['out']) + biases['out']


if data_rep == 1:
    pred = RNN_1D(x, weights, biases)
else:
    pred = RNN_ND(x, weights, biases)
    data = arr

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.RMSPropOptimizer(learning_rate=lr).minimize(cost)

correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(init)
    step = 0
    offset = random.randint(0, n_inputs+1)
    end_offset = n_inputs + 1
    acc_total = 0
    loss_total = 0

    writer.add_graph(session.graph)

    while step < training_iters:
        if offset > (len(data) - end_offset):
            offset = random.randint(0, n_inputs+1)
        batches_x = []
        batches_y = []
        for k in range(batch_size):
            batches_x.append([data[k+i] for i in range(offset, offset+n_inputs)])
            if data_rep == char_size:
                batches_y.append([data[offset+n_inputs+k]])
            else:
                symbols_y_onehot = np.zeros([char_size], dtype=float)
                symbols_y_onehot[int(data[offset+n_inputs+k])] = 1.0
                batches_y.append(symbols_y_onehot)
        batches_x = np.reshape(np.array(batches_x), [-1, n_inputs, data_rep])
        batches_y = np.reshape(batches_y, [-1, char_size])

        _, acc, loss, onehot_pred = session.run([optimizer, accuracy, cost, pred],
                                                feed_dict={x: batches_x, y: batches_y})

        step += 1
        offset += batch_size



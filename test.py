from __future__ import print_function, division # python 3.x compatible

import urllib2
from bs4 import BeautifulSoup
import pandas as pd
import re
from unidecode import unidecode

# specify the url
# quote_page = "http://www.bloomberg.com/quote/PSX:IND"
# page = urllib2.urlopen(quote_page)

# define the name of the songs page
quote_page = "http://www.songfacts.com/artist-drake.php"
page = urllib2.urlopen(quote_page) # load the page
soup = BeautifulSoup(page, "html.parser") # load the page to beautifulsoup
songs = soup.find("ul", attrs={"class": "songullist-orange"}).findAll("a") # parse the gongullist
songs_hyphen = []
# iterate over all songs and change name to the x-x-x pattern
for song in songs:
    song_words = [re.sub('[^A-Za-z0-9]+', '', w) for w in song.text.lower().split(' ') \
                  if re.sub('[^A-Za-z0-9]+', '', w) != ""]
    songs_hyphen.append("-".join(song_words))

# Define the lyric page and use BS4 to parse all the lyrics
lyric_page = 'http://metrolyrics.com/{}-lyrics-drake.html'
lyrics_list = []
for song in songs_hyphen:
    page = urllib2.urlopen(lyric_page.format(song))
    soup = BeautifulSoup(page, "html.parser")
    verses = soup.find_all('p', attrs={'class': 'verse'})
    lyrics = ''
    for verse in verses:
        old_text = verse.text.strip()
        # print(text)
        # print("-----------------------------------------------")
        text = re.sub(r"\[.*\]\n", "", unidecode(old_text))
        if old_text != text:
            print(False)
        if lyrics == "":
            lyrics = lyrics + text.replace('\n', '|-|')
        else:
            lyrics = lyrics + '|-|' + text.replace('\n', '|-|')
    lyrics_list.append(lyrics + '|-|')

songs_dict = {
    "songs": songs_hyphen,
    "lyrics": lyrics_list
}

songs_df = pd.DataFrame(songs_dict)
songs_df.to_csv("lyrics.csv")


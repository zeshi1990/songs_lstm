import numpy as np
import random

from utils import read_lyrics

n_inputs = 20
batch_size = 10

arr, arr_1d, char_2_idx, idx_2_char = read_lyrics("lyrics.csv")

char_size = arr.shape[1]
data = arr_1d
if len(data.shape) == 1:
    data_rep = 1
else:
    data_rep = char_size
print(data_rep)

offset = random.randint(0, n_inputs+1)
end_offset = offset + n_inputs
if offset > (len(data) - end_offset):
    offset = random.randint(0, n_inputs + 1)
batches_x = []
batches_y = []
for k in range(batch_size):
    batches_x.append([data[k + i] for i in range(offset, offset + n_inputs)])
    if data_rep == char_size:
        batches_y.append([data[offset + n_inputs + k]])
    else:
        symbols_y_onehot = np.zeros([char_size], dtype=float)
        symbols_y_onehot[int(data[offset + n_inputs + k])] = 1.0
        batches_y.append(symbols_y_onehot)
batches_x = np.reshape(np.array(batches_x), [-1, n_inputs, data_rep])
batches_y = np.reshape(batches_y, [-1, char_size])

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--use_fp16',
                      default=False,
                      help='Use half floats instead of full floats if True.',
                      action='store_true')
parser.add_argument('--self_test',
                    default=False,
                    action='store_true',
                    help='True if running a self test.')
FLAGS, unparsed = parser.parse_known_args()
print(FLAGS)
print(unparsed)

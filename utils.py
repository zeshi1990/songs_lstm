import numpy as np
import pandas as pd
import string


def char_one_hot(strg, alphabet=string.ascii_lowercase + string.punctuation):
    alpha_2_idx = {}
    idx_2_alpha = {}
    for i, char in enumerate(alphabet):
        alpha_2_idx[char] = i
        idx_2_alpha[i] = char
    arr = np.zeros((len(strg), len(alphabet)))
    arr_1d = np.zeros(len(strg))
    for i, char in enumerate(strg):
        if char in alpha_2_idx:
            arr[i, alpha_2_idx[char]] = 1
            arr_1d[i] = alpha_2_idx[char]
    return arr, arr_1d, alpha_2_idx, idx_2_alpha


def read_lyrics(fn):
    # Read the lyrics csv
    df = pd.read_csv(fn, index_col=0)
    text = ""
    for i, row in df["lyrics"].iteritems():
        if row == "|-|":
            print("Found an empty lyric")
            continue
        text += row

    # Take the lower case of all text
    text = text.lower()

    # One hot encoding of the lyrics
    return char_one_hot(text)
